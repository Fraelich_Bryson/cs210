/** pa3functs.h
* ================================================================
* Name: Bryson Fraelich
* Section: xxx
* Project: Programming Assessment #3 - header file
* =================================================================
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct MP3_HEADER_STRUCT{
    char header[3];
    char title[30];
    char artist[30];
    char album[30];
    char year[4];
    char comment[30];
    char genre[1];
} MP3_HEADER;

#ifndef MYEXE_PA3FUNCTS_H
#define MYEXE_PA3FUNCTS_H

void getFileName(char *fileName);

MP3_HEADER readHeader(char *fileName);

void displayMP3Header(MP3_HEADER *header);

int updateCommentField(char *fileName, char *comment, MP3_HEADER header);

int countLetter(char *field, char letter);

MP3_HEADER* mp3Library(int numSongs);

int writeNewFile(MP3_HEADER* MP3_Library, int numSongs);

#endif //MYEXE_PA3FUNCTS_H
