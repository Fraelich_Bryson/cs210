/** pa3main.c
* ================================================================
* Name: Bryson Fraelich
* Section: xxx
* Project: Programming Assessment #3 - main file
* =================================================================
*/
#include "pa3functs.h"

int main() {
    char fileName[30];

    getFileName(fileName);

    MP3_HEADER header = readHeader(fileName);

    displayMP3Header(&header);

    char newComment[30];
    printf("Enter new comment\n");
    scanf(" %s", newComment);
    updateCommentField(fileName, newComment, header);

    char field[10];
    char c;
    printf("Enter field:\n");
    scanf(" %s", field);
    printf("Enter character:\n");
    scanf(" %c", &c);
    countLetter(field, c);

//    int numSongs;
//    printf("Enter number of songs:\n");
//    scanf(" %d", &numSongs);
//    MP3_HEADER *library = (MP3_HEADER*)malloc(sizeof(MP3_HEADER) * numSongs);
//    mp3Library(numSongs);

//    int written = writeNewFile(mp3Library(numSongs), numSongs);
//    printf("Written: %d\n", written);

    return 0;
}