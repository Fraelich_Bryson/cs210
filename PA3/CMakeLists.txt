cmake_minimum_required(VERSION 3.12)
project(PA3)

set(CMAKE_CXX_STANDARD 14)

add_executable(pa3functs.h pa3functs.c pa3main.c)