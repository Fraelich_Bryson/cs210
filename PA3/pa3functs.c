/** pa3functs.c
* ================================================================
* Name: Bryson Fraelich
* Section: xxx
* Project: Programming Assessment #3 - function file
* =================================================================
*/

#include "pa3functs.h"

void getFileName(char *fileName) {
    printf("Enter a file name:\n");
    scanf(" %s", fileName);
    FILE *in = fopen(fileName, "rb");
    if(in == NULL){
        printf("Error\n");
        getFileName(fileName);
    }
    fclose(in);
}

MP3_HEADER readHeader(char *fileName) {
    MP3_HEADER *header = NULL;
    header = malloc(sizeof(MP3_HEADER));

    FILE *in = NULL;
    in = fopen(fileName, "rb");
    if (in == NULL) {
        printf("Error\n");
    }

    // Tried reading from the end of the file until 128 bits
    fseek(in, 128, SEEK_END);
    fread(header->genre, 1, 1, in);
    fread(header->comment, 30, 1, in);
    fread(header->year, 4, 1, in);
    fread(header->album, 30, 1, in);
    fread(header->artist, 30, 1, in);
    fread(header->title, 30, 1, in);
    fread(header->header, 3, 1, in);

//    fread(header->header, 3, 1, in);
//    fread(header->title, 30, 1, in);
//    fread(header->artist, 30, 1, in);
//    fread(header->album, 30, 1, in);
//    fread(header->year, 4, 1, in);
//    fread(header->comment, 30, 1, in);
//    fread(header->genre, 1, 1, in);

    fclose(in);
    return *header;
}

void displayMP3Header(MP3_HEADER *header){

    printf("Title: %s\n", header->title);
    printf("Artist: %s\n", header->artist);
    printf("Album: %s\n", header->album);
    printf("Year: %s\n", header->year);
    printf("Comment: %s\n", header->comment);

}

int updateCommentField(char *fileName, char *comment, MP3_HEADER header){
    int written = 0;

    return written;
}

int countLetter(char *field, char letter){
    int count = 0;
    int len = strlen(field);

    for(int i = 0; i < len; ++i){
        if(field[i] == letter){
            ++count;
        }
    }

    return count;
}

//MP3_HEADER* mp3Library(int numSongs){
//
//}

//int writeNewFile(MP3_HEADER* MP3_Library, int numSongs){
//    FILE* out = NULL;
//    out = fopen("outputFile.txt", "w");
//
//    if (out == NULL){
//        printf("Error\n");
//    }
//
//    for(int i = 0; i < numSongs; ++i){
//        fprintf(out, MP3_Library[i].header);
//        fprintf(out, MP3_Library[i].title);
//        fprintf(out, MP3_Library[i].artist);
//        fprintf(out, MP3_Library[i].album);
//        fprintf(out, MP3_Library[i].year);
//        fprintf(out, MP3_Library[i].comment);
//        fprintf(out, MP3_Library[i].genre);
//    }
//}