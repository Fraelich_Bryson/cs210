/** ===============================================================================================
 * ***********************************************************************************************
 * ================================================================================================
 * Name: Bryson Fraelich, 11.25.19
 * Section: T1/2A
 * Project: Pong
 * Purpose: functs.c file for all of the relevant functions in pong.exe
 * ===============================================================================================
 * ***********************************************************************************************
 * ===============================================================================================
 */

#include "main.h"

/* Creates a window when the game is first started. The window
 * prompts the users for names pertaining to player1 and player2.
 * It then stores these names in corresponding player structs.
 * The screen also shows the instructions for player movement.
 */
void new_game(Player *player1, Player *player2) {
    echo();
    WINDOW *New_Game = newwin(40, 118, 0, 1);
    refresh();
    box(New_Game, ACS_VLINE, ACS_HLINE);

    mvwprintw(New_Game, 3, 37, "INSTRUCTIONS:");
    mvwprintw(New_Game, 4, 37, "-------------");
    mvwprintw(New_Game, 6, 37, "PLAYER 1 MOVEMENT: up is 'a' && down is 'z'");
    mvwprintw(New_Game, 8, 37, "PLAYER 2 MOVEMENT: up is UP_KEY && down is DOWN_KEY");
    mvwprintw(New_Game, 10, 37, "END MATCH: 'g'");

    mvwprintw(New_Game, 19, 48, " ----------");
    mvwprintw(New_Game, 20, 48, "| NEW GAME |");
    mvwprintw(New_Game, 21, 48, " ----------");
    mvwprintw(New_Game, 24, 42, "ENTER PLAYER 1: ");
    wgetstr(New_Game, player1->name);
    mvwprintw(New_Game, 26, 42, "Enter Player 2: ");
    wgetstr(New_Game, player2->name);
    wrefresh(New_Game);
}

/* Initializes Player 1, Player 2, and the Ball x and y positions.
 * Also sets the score to 0 for each new match played. The ball's
 * velocity is set for the x and y direction for each new match.
 * calls the function that will create the status area, players,
 * and ball.
 */
void initialize(Player *player1, Player *player2, Ball *ball) {
    player1->y_1 = 13;
    player2->y_1 = 13;

    player1->score = 0;
    player2->score = 0;

    player1->x = 3;
    player2->x = 115;

    ball->x = 56;
    ball->y = 13;

    ball->x_velocity = -2;
    ball->y_velocity = -1;

    Draw_Status_Area(player1, player2);
    Draw_Board(player1, player2, ball);
}

/* Draws the status area by creating the Status_Area window to print the
 * player and their score. Also gives the instructions on how to leave
 * the game before the minimum score is reached. Updates the player's names
 * to have a title.
 */
void Draw_Status_Area(Player *player1, Player *player2) {
    WINDOW *Status_Area = newwin(7, 118, 0, 1);
    refresh();
    box(Status_Area, ACS_VLINE, ACS_HLINE);

    // Adds the title [DEFENDER] to each player's name.
    char defender1[25] = "[DEFENDER] ";
    strcat(defender1, player1->name);
    char defender2[25] = "[DEFENDER] ";
    strcat(defender2, player2->name);

    mvwprintw(Status_Area, 3, 25, "%s", defender1);
    mvwprintw(Status_Area, 4, 25, "SCORE: %d", player1->score);
    mvwprintw(Status_Area, 3, 79, "%s", defender2);
    mvwprintw(Status_Area, 4, 79, "SCORE: %d", player2->score);
    mvwprintw(Status_Area, 2, 45, "-- Hit 'g' to exit --");
    wrefresh(Status_Area);
}

/* Draws the board by using the Board window and calling the
 * draw_player_1, draw_player_2, and draw_board functions. Also
 * creates a box around the board window.
 */
void Draw_Board(Player *player1, Player *player2, Ball *ball) {
    WINDOW *Board = newwin(33, 118, 7, 1);
    refresh();
    box(Board, ACS_VLINE, ACS_HLINE);
    wrefresh(Board);

    Draw_Player_1(Board, player1);
    Draw_Player_2(Board, player2);
    Draw_Ball(Board, ball);
}

/* Draws player1 by using the board window and printing 5 lines
 * based on player1's y_1 position. Player1's x position does not
 * change.
 */
void Draw_Player_1(WINDOW *board, Player *player1) {
    player1->y_2 = player1->y_1 + 1;
    player1->y_3 = player1->y_1 + 2;
    player1->y_4 = player1->y_1 + 3;
    player1->y_5 = player1->y_1 + 4;

    mvwprintw(board, player1->y_1, player1->x, "|");
    mvwprintw(board, player1->y_2, player1->x, "|");
    mvwprintw(board, player1->y_3, player1->x, "|");
    mvwprintw(board, player1->y_4, player1->x, "|");
    mvwprintw(board, player1->y_5, player1->x, "|");
    wrefresh(board);
    move(0, 0);
}

/* Draws player2 by using the board window and printing 5 lines
 * based on player2's y_1 position. Player2's x position does not
 * change.
 */
void Draw_Player_2(WINDOW *board, Player *player2) {
    player2->y_2 = player2->y_1 + 1;
    player2->y_3 = player2->y_1 + 2;
    player2->y_4 = player2->y_1 + 3;
    player2->y_5 = player2->y_1 + 4;

    mvwprintw(board, player2->y_1, player2->x, "|");
    mvwprintw(board, player2->y_2, player2->x, "|");
    mvwprintw(board, player2->y_3, player2->x, "|");
    mvwprintw(board, player2->y_4, player2->x, "|");
    mvwprintw(board, player2->y_5, player2->x, "|");
    wrefresh(board);
    move(0, 0);
}

/* Draws the ball by using the board window and printing an "O"
 * onto the screen.
 */
void Draw_Ball(WINDOW *board, Ball *ball) {
    mvwprintw(board, ball->y, ball->x, "O");
    wrefresh(board);
    move(0, 0);
}


/* Creates a new window that covers the screen, with a box as a border.
 * The screen states that the match is over and a player has won. It
 * then promts the user to enter either 'y' or 'n' for if they want to
 * play again. The function then calls the user_input function to get
 * their answer.
 */
int End_Game(Player winner) {
    clear(); // clears the screen
    WINDOW *END_SCREEN = newwin(40, 118, 0, 1);
    refresh();
    box(END_SCREEN, ACS_VLINE, ACS_HLINE);
    mvwprintw(END_SCREEN, 7, 53, " ----------");
    mvwprintw(END_SCREEN, 8, 53, "| END GAME |");
    mvwprintw(END_SCREEN, 9, 53, " ----------");
    mvwprintw(END_SCREEN, 13, 54, "%s WON!", winner.name);
    mvwprintw(END_SCREEN, 22, 40, "WANT TO PLAY AGAIN?  ' y / n' ");
    wrefresh(END_SCREEN);

    // Checks for user input and if they want they can play again
    return user_input();
};

/* Creates a new window that covers the screen, with a box as a border.
 * The screen prompts that the match is over and no player won. It
 * then prompts the user to enter either 'y' or 'n' to if they want to
 * play again. The function then calls the user_input function to get
 * their answer.
 */
int End_Game_2() {
    clear(); // clears the screen
    WINDOW *END_SCREEN = newwin(40, 118, 0, 1);
    refresh();
    box(END_SCREEN, ACS_VLINE, ACS_HLINE);
    mvwprintw(END_SCREEN, 7, 53, " ----------");
    mvwprintw(END_SCREEN, 8, 53, "| END GAME |");
    mvwprintw(END_SCREEN, 9, 53, " ----------");
    mvwprintw(END_SCREEN, 13, 54, "NO WINNER");
    mvwprintw(END_SCREEN, 22, 40, "WANT TO PLAY AGAIN?  ' y / n '");
    wrefresh(END_SCREEN);

    // Checks for user input and if they want they can play againn
    return user_input();
};

/* This function recursively asks what the would like to do, which is
 * to continue playing the game by starting a new match, or ending the game.
 */
int user_input() {
    if (getch() == 'y') {
        return 0;
    } else if (getch() == 'n') {
        return 1;
    } else {
        return user_input();
    }
}

/* Creates a file pointer and opens the game_stats file. The game information is saved to that file.
 * This includes the number of games played, the names of both players, their total individual points
 * added up from each match, and the number of games they won. It then closes the file.
 */
void save_game_info(Game_Info *Game, Player *player1, Player *player2) {
    FILE *game_stats_file = NULL;
    game_stats_file = fopen(
            "C:\\Users\\C22Bryson.Fraelich\\CLionProjects\\CS210\\Pong\\cmake-build-debug\\game_stats.txt", "w");
    if (game_stats_file == NULL) {
        printf("\nCould not open file.\n");
    }

    fprintf(game_stats_file, "Number of Games Played: %d\n", Game->num_games);
    fprintf(game_stats_file, "[Player 1 Info] Name: %s    Points: %d    Games Won: %d\n", player1->name,
            player1->total_points, player1->games_won);
    fprintf(game_stats_file, "[Player 2 Info] Name: %s    Points: %d    Games Won: %d\n", player2->name,
            player2->total_points, player2->games_won);

    fclose(game_stats_file);
}
