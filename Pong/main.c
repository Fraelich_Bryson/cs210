/** =============================================================================================
* ***********************************************************************************************
* ===============================================================================================
*                    ,-.----.        ,----..             ,--.
*                    \    /  \      /   /   \          ,--.'|   ,----..
*                    |   :    \    /   .     :     ,--,:  : |  /   /   \
*                    |   |  .\ :  .   /   ;.  \ ,`--.'`|  ' : |   :     :
*                    .   :  |: | .   ;   /  ` ; |   :  :  | | .   |  ;. /
*                    |   |   \ : ;   |  ; \ ; | :   |   \ | : .   ; /--`
*                    |   : .   / |   :  | ; | ' |   : '  '; | ;   | ;  __
*                    ;   | |`-'  .   |  ' ' ' : '   ' ;.    ; |   : |.' .'
*                    |   | ;     '   ;  \; /  | |   | | \   | .   | '_.' :
*                    :   ' |      \   \  ',  /  '   : |  ; .' '   ; : \  |
*                    :   : :       ;   :    /   |   | '`--'   '   | '/  .'
*                    |   | :        \   \ .'    '   : |       |   :    /
*                    `---'.|         `---`      ;   |.'        \   \ .'
*                      `---`                    '---'           `---`
* ===============================================================================================
* ***********************************************************************************************
* ===============================================================================================
 * Name: Bryson Fraelich, 11.25.19
 * Section: T1/2A
 * Project: Pong
 * Purpose: A two player version of the classic PONG game. Players enter their name in the first
 * screen. The second screen that comes up has two different sections. The top portion shows the
 * player names and their scores. The bottom portion is the game board and has player1's paddle
 * on the left and player2's paddle on the right. The ball starts moving and players have to hit
 * the ball by moving the paddle, using their coorasponding keys, to the perceived next place
 * that the ball will land on their side. Each match is only 1 point. Player's have the option to
 * end the current game at any time by pressing the 'g' key. Once a match has finished players
 * decide if they want to continue by playing another game, or to end the game entirely. Both of
 * the player's information: name, total points, and matches won, as well as the total games play
 * -ed are saved to a file to view for later.
 *
 *                  (--- THE SCREEN IS SIZED TO 120 X 40 FOR CYGWIN ---)
* ===============================================================================================
* ***********************************************************************************************
* ===============================================================================================
*/

/* ===============================================================================================
 *                                DOCUMENTATION
 * ===============================================================================================
 * I used multiple sources (EI, articles, wikis, and youtube vidoes) to show me how to use
 * curses properly. This included how to start curses, create different windows, refresh the screen,
 * print to specific screens, enable the keyboard, and draw a box around each window. Among the
 * sources used were the following:
 * - lt. Col. Sarmiento helped me by showing me that ncurses could be used to make a screen for my
 *   program. She helped me by explaining that the pong.exe could be "dragged and dropped" into the
 *   cygwin terminal to run it.
 * - Major Dressler helped me with the velocity portion of the ball. He explained that to get proper
 *   ball movement it would be best to give the ball a general direction of movement and then update
 *   it when it came into contact with another surface. Additionally, he showed me a way to keep the
 *   ball moving while there were no keys pressed was by continually looping through the ball movement
 *   and then making the player move when there was a key pressed, and not the other way around. He
 *   showed me that I could use recursion in the end_game screen by asking for user input and recursively
 *   waiting for either a 'y' or 'n' to be entered. Finally, he explained that a simple way to do
 *   string manipulation was by adding a title to the player names, like "Defender."
 * - http://patorjk.com/software/taag/#p=display&h=0&f=3D%20Diagonal&t=PONG was used for the ascii
 *   art in the main page header.
 * - https://www.gnu.org/software/ncurses/ncurses-intro.html#terminology was used to help me with
 *   ncurses setup and general use.
 * - https://www.youtube.com/watch?v=6v_5i8NpyUM was used to show me how to use mvprintw to add
 *   information to the screen. I used this for every portion of the program that prints any text
 *   to the screen.
 * - http://tldp.org/HOWTO/NCURSES-Programming-HOWTO/ was used for multiple curses functions and
 *   general use.
 * - https://www.youtube.com/watch?v=pjT5wq11ZSE&list=PL2U2TQ__OrQ8jTf0_noNKtHMuYlyxQl4v&index=3
 *   showed me what the differences in curses was between refresh vs wrefresh.
 * - https://stackoverflow.com/questions/15450682/ncurses-program-exits-when-terminal-resized was used
 *   to show me how to make it so the terminal won't close if it is resized.
 * - https://stackoverflow.com/questions/28328982/using-curses-how-do-i-update-the-screen-or-wait-for-a-key
 *   showed me that halfdelay() could be used to create an input delay for a set amount of time. I implemented
 *   this into the ball movement loop by only delaying the time between checking for user input by halfdelay(1);
 * - https://www.mkssoftware.com/docs/man3/curs_getstr.3.asp was used to show me how to get a string
 *   from the user by utilizing wgetstr(window, y, x, str).
 * - http://tldp.org/HOWTO/NCURSES-Programming-HOWTO/scanw.html helped me by showing me that I could
 *   print to the screen in ncurses by using the standard "%s".
 * - http://transit.iut2.upmf-grenoble.fr/cgi-bin/man/man2html?getch+3 showed me that getch() returns
 *   ERR if nothing is input. This allowed me to continually print the screen while the input was ERR.
 */

#include "main.h"

int main(int argc, char *argv[]) {
    /* Allocates the memory for the players 1 and 2, the ball, and game, using their corresponding
     * structs. The game state is initialized to 0 so the game loop will initiate. The counter for
     * the number of games, player1 and player2 total points, and the number of games each player
     * has won are all started at 0.
         */
    Player *player1 = (Player *) malloc(sizeof(Player));
    Player *player2 = (Player *) malloc(sizeof(Player));
    Ball *ball = (Ball *) malloc(sizeof(Ball));
    Game_Info *Game = (Game_Info *) malloc(sizeof(Game_Info));
    Game->game_state = 0;
    Game->num_games = 0;
    player1->total_points = 0;
    player2->total_points = 0;
    player1->games_won = 0;
    player2->games_won = 0;

    /* This loop is controlled by the game state variable and allows for the game to
     * continue after each match is completed.
     */
    while (Game->game_state == 0) {
        initscr(); // Initializes the curses screen
        keypad(stdscr, TRUE); // Enables keyboard mapping

        /* For the first match that is played the new_game function is called, which opens
         * the starting window that displays the player instructions and prompts each
         * player for their name.
         */
        if (Game->num_games == 0) {
            new_game(player1, player2);
        }

        // Creates a delay so the screen can print the ball when no input is detected.
        halfdelay(1);

        // Makes it so none of the keys entered get printed to the screen.
        noecho();

        /* Calls the initialize function to call the other required
         * functions to create the starting positions for the ball,
         * players, and screen.
         */
        initialize(player1, player2, ball);

        int match_state = 0; // Initializes the current match state to 0.

        /* This loop continually updates the position and velocity of the ball for
         * both x and y directions. It then checks for player movement, which corresponds
         * to either player1 or player2 key binds, and then moves the player accordingly.
         * The positions of both paddles in coordination with the ball position is checked
         * to see if there are any impacts made, and the ball's velocity is updated. If the
         * paddles are not in the corresponding location, to the ball, the score is updated.
         * The end condition is if a player reaches 1 points or 'g' is entered.
         */
        while (match_state != 1) {

            // Checks to see if the player wishes to quit the game.
            if (getch() == 'g') {
                match_state = 1;
            }

            refresh();
            // Updates the ball's position with respect to each axis and the velocity.
            ball->x = ball->x + ball->x_velocity;
            ball->y = ball->y + ball->y_velocity;

            // If the ball reaches the top or bottom boundary it's x-velocity is reversed.
            if (ball->y <= 2) {
                ball->y_velocity = ball->y_velocity * -1;
            }
            if (ball->y >= 31) {
                ball->y_velocity = ball->y_velocity * -1;
            }

            /* Checks for when the ball's x-position is 1 off from player1's paddle and if one of the
             * y-positions are the same. This would be a block by that player and the ball's
             * x-velocity would be reversed.
             */
            if ((ball->y == player1->y_1 || ball->y == player1->y_2 || ball->y == player1->y_3 ||
                 ball->y == player1->y_4 ||
                 ball->y == player1->y_5) && ball->x == player1->x + 1) {
                ball->x_velocity = ball->x_velocity * -1;
            }

            /* Checks for when the ball's x-position is at player1's x-position or less and that
             * the paddle is not there, then updates the player's current score for the match and
             * their total points throughout the game. The ball is then reset at the center of the
             * bottom screen and the status area is updated to reflect the new points.
             */
            if ((ball->y != player1->y_1 || ball->y != player1->y_2 || ball->y != player1->y_3 ||
                 ball->y != player1->y_4 ||
                 ball->y != player1->y_5) && ball->x <= player1->x) {
                ++player2->score;
                ++player2->total_points;
                ball->x = 56;
                ball->y = 13;
                Draw_Status_Area(player1, player2);
            }

            /* Checks for when the ball's x-position is 1 off from player2's paddle and if one of the
             * y-positions are the same. This would be a block by that player and the ball's
             * x-velocity would be reversed.
             */
            if ((ball->y == player2->y_1 || ball->y == player2->y_2 || ball->y == player2->y_3 ||
                 ball->y == player2->y_4 ||
                 ball->y == player2->y_5) && ball->x == player2->x - 1) {
                ball->x_velocity = ball->x_velocity * -1;
            }

            /* Checks for when the ball's x-position is at player2's x-position or less and that
             * the paddle is not there, then updates the player's current score for the match and
             * their total points throughout the game. The ball is then reset at the center of the
             * bottom screen and the status area is updated to reflect the new points.
             */
            if ((ball->y != player2->y_1 || ball->y != player2->y_2 || ball->y != player2->y_3 ||
                 ball->y != player2->y_4 ||
                 ball->y != player2->y_5) && ball->x >= player2->x) {
                ++player1->score;
                ++player1->total_points;
                ball->x = 56;
                ball->y = 13;
                Draw_Status_Area(player1, player2);
            }

            // Ends the current match loop when a certain score has been reached by either player.
            if ((player1->score == 1) || (player2->score == 1)) {
                match_state = 1;
            }

            // Draws the board
            Draw_Board(player1, player2, ball);

            /* Waits until a key is entered and then reacts accordingly. This allows for
             * continues ball movement on the screen because it's not waiting for player
             * commands to update the ball.
             */
            if (getch() != ERR) {

                /* Player 1 Movement
                 * If the 'a' key is pressed then player1 is moved up.
                 * If the 'z' key is pressed then player1 is moved down.
                 */
                if (getch() == 'a' && player1->y_1 > 1) {
                    --player1->y_1;
                }
                if (getch() == 'z' && player1->y_1 < 27) {
                    ++player1->y_1;
                }

                /* Player 2 Movement
                 * If the KEY_UP is pressed then player2 is moved up.
                 * If the KEY_DOWN is pressed then player2 is moved down.
                 */
                if (getch() == KEY_UP && player2->y_1 > 1) {
                    --player2->y_1;
                }
                if (getch() == KEY_DOWN && player2->y_1 < 27) {
                    ++player2->y_1;
                }

                // Draws the screen and moves the cursor to the top left of the screen.
                Draw_Board(player1, player2, ball);
                move(0, 0);
            }
        }

        /* If a player won the match then that match is ended. That player's number of
         * matches won and total matches played is incremented. the game_state is
         * updated after calling the End_Game function, which asks if they would like to
         * play another match. The same is accomplished if the game was terminated early,
         * but not won games is updated.
         */
        if (player1->score == 1) {
            Game->game_state = End_Game(*player1);
            ++player1->games_won;
            ++Game->num_games;
        } else if (player2->score == 1) {
            Game->game_state = End_Game(*player2);
            ++player2->games_won;
            ++Game->num_games;
        } else {
            Game->game_state = End_Game_2();
        }

    }

    // The game info is output to a game stats file in the folder.
    save_game_info(Game, player1, player2);

    // Clears the memory used for each player, the ball, and the game so far.
    free(player1);
    free(player2);
    free(ball);
    free(Game);

    // Closes the terminal.
    endwin();

    return 1;
}
