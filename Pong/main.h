/** ===============================================================================================
 * ***********************************************************************************************
 * ================================================================================================
 * Name: Bryson Fraelich, 11.25.19
 * Section: T1/2A
 * Project: Pong
 * Purpose: Header file which defines 2 structs and all functions
 * ===============================================================================================
 * ***********************************************************************************************
 * ===============================================================================================
 */

#include <curses.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <string.h>

#ifndef PONG_MAIN_H
#define PONG_MAIN_H

/** Structure used for both player1 and player2. Keeps track
 * of each player's name, their x and y positions, their score
 * for the current match, total points they have achieved in the
 * game so far, and the number of matches won.
 */
typedef struct player_struct {
    char name[15];
    int y_1;
    int y_2;
    int y_3;
    int y_4;
    int y_5;
    int x;
    int score;
    int total_points;
    int games_won;
} Player;

/** Structure used for the ping pong ball. Keeps track of the
 * ball's x and y position and the velocity for both directions.
 */
typedef struct ball_struct {
    int x;
    int y;
    int x_velocity;
    int y_velocity;
} Ball;

/** Structure used for the game information. It keeps track
 * of the current game state (0 is going and 1 is stopped).
 * Also keeps count of how many games have been played during
 * this iteration.
 */
typedef struct game_info_struct {
    int game_state;
    int num_games;
} Game_Info;

/** ----------------------------------------------------------
* Creates a window when the game is first started. The window
* prompts the users for names pertaining to player1 and player2.
* It then stores these names in corresponding player structs.
* @param ptr to player1 and ptr to player2
* @return None.
*/
void new_game(Player *player1, Player *player2);

/** ----------------------------------------------------------
* Initializes Player 1, Player 2, and the Ball x and y position.
* Calls the functions to create the screen and windows. Also
* calls the function that will create the status area, players,
* and ball.
* @param ptr to player1, ptr to player2, and ptr
* to ball.
* @return None.
*/
void initialize(Player *player1, Player *player2, Ball *ball);

/** ----------------------------------------------------------
* Draws the status area by using the Status_Area window to print the
* player and their score. Also gives the instructions on how to leave.
* @param ptr to player1 and ptr to player2.
* @return None.
*/
void Draw_Status_Area(Player *player1, Player *player2);

/** ----------------------------------------------------------
* Draws the board by using the Board window and calling the
 * draw_player_1, draw_player_2, and draw_board functions.
* @param ptr to player1, ptr to player2, and ptr
* to the ball.
* @return None.
*/
void Draw_Board(Player *player1, Player *player2, Ball *ball);

/** ----------------------------------------------------------
* Draws player1 by using the board window and printing 5 lines
* based on player1's y position. Player1's x position does not
* change.
* @param ptr to the board window and ptr to player2.
* @return None.
*/
void Draw_Player_1(WINDOW *board, Player *player1);

/** ----------------------------------------------------------
* Draws player2 by using the board window and printing 5 lines
* based on player2's y position. Player2's x position does not
* change.
* @param ptr to the board window and ptr to player2.
* @return None.
*/
void Draw_Player_2(WINDOW *board, Player *player2);

/** ----------------------------------------------------------
* Draws the ball by using the board window and printing an "O"
* onto the screen.
* @param ptr to Board window and ptr to the ball.
* @return None.
*/
void Draw_Ball(WINDOW *board, Ball *ball);

/** ----------------------------------------------------------
* Creates a new window that covers the screen, with a box as a border.
* The screen states that the match is over and a player has won. It
* then prompts the user to enter either 'y' or 'n' for if they want to
* play again. The function then calls the user_input function to get
* their answer.
* @param winner
* @return An integer that updates the game_sate.
*/
int End_Game(Player winner);

/** ----------------------------------------------------------
* Creates a new window that covers the screen, with a box as a border.
* The screen states that the match is over and no player has won. It
* then prompts the user to enter either 'y' or 'n' for if they want to
* play again. The function then calls the user_input function to get
* their answer.
* @param None.
* @return An integer that updates the game_sate.
*/
int End_Game_2();

/** ----------------------------------------------------------
* This function recursively asks what the would like to do, which is
* to continue playing the game by starting a new match, or ending the game.
* @param None.
* @return None.
*/
int user_input();

/** ----------------------------------------------------------
* Creates a file pointer and opens the game_stats file. The game information is saved to that file.
* This includes the number of games played, the names of both players, their total individual points
* added up from each match, and the number of games they won. It then closes the file.
* @param ptr to Game, ptr to player1, and ptr to player2.
* @return None.
*/
void save_game_info(Game_Info *Game, Player *player1, Player *player2);

#endif //PONG_MAIN_H
