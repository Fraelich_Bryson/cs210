/** pa4functs.h
* ===========================================================
* Name: BRYSON FRAELICH, 21.11.19
* Section: T1/2A
* ===========================================================  */

#ifndef MYEXE_PA4FUNCTS_H
#define MYEXE_PA4FUNCTS_H

#include <stdint.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

int nth_even(int N);

int almost_fibonacci(int N);

int alternating_cubes(int N);

int is_positive(int x);

int mult_by_two(int x);

int set_bit(int val, int N, int bitVal);

int quick_hash(char message[]);

void print_bits(void* ptr, int num_bytes);

#endif //MYEXE_PA4FUNCTS_H
