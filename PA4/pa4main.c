/** pa4main.c
* ===========================================================
* Name: BRYSON FRAELICH, 21.11.19
* Section: T1/2A
* ===========================================================  */

#include "pa4functs.h"

int main() {
    
    // Exercise 1
    int x = 2;
    printf("%d's even is %d\n", x, nth_even(x));

    // Exercise 3
    int y = 4;
    printf("Alternating cube of %d is %d\n", y, alternating_cubes(y));

    // Exercise 4
    printf("%d is positive (1) or negative (0): %d\n", x, is_positive(x));

    // Exercise 5
    printf("2 * %d is %d\n", x, mult_by_two(x));

    // Exercise 6
    x = 0xffd67472;
    y = 6;
    int z = 1;

    int new = set_bit(x, y, z);

    print_bits(&x, 4);
    print_bits(&new, 4);

    // Exercise 7
    printf("hash of %c is %d\n", 'A', quick_hash("A"));
    
    return 0;
}