/** pa4functs.c
* ===========================================================
* Name: BRYSON FRAELICH, 21.11.19
* Section: T1/2A
* ===========================================================  */

#include "pa4functs.h"

int nth_even(int N) {
    if(N == 1){
        return 2;
    }

    return 2 + nth_even(N - 1);
}

int almost_fibonacci(int N) {
    if (N == 1) {
        return 5;
    } else if (N == 2) {
        return 1;
    }

    return almost_fibonacci(N - 1) + almost_fibonacci(N - 2);
}

int alternating_cubes(int N) {
    if (N == 0) {
        return 0;
    }
    if (N % 2 != 0) {
        return (-1 * pow(N, 3)) + alternating_cubes(N - 1);
    } else {
        return pow(N, 3) + alternating_cubes(N - 1);
    }
}

int is_positive(int x) {
    return ((x >> 31) & 1) ^ 1;
}

int mult_by_two(int x) {
    return (x << 1);
}

int set_bit(int val, int N, int bitVal) {
        // Tried shifting the nth bit of the value to the right to check if it already matched the bitVal
        // If not, then it would shift the bitVal over Nth times and swap the bit
        if(((val >> (N - 1)) == bitVal)){
            return val;
        } else {
            return val ^ (1 << (N - 1));
        }
}

int quick_hash(char message[]) {
    int hash = 0x00000000;
    int i;

    for (i = 0; i < strlen(message); ++i) {
        hash = hash ^ (message[i]);
        hash = hash ^ (hash << 10);
        hash = hash ^ (hash >> 6);
    }

    hash = hash ^ (hash << 3);
    hash = hash ^ (hash >> 11);
    hash = hash ^ (hash << 15);

    return hash;
}



void print_bits(void* ptr, int num_bytes) {
    // Cast the pointer as an unsigned byte
    uint8_t* byte = ptr;

    // For each byte, (bytes are little endian ordered)
    for (int i = num_bytes - 1; i >= 0; --i) {

        // For each bit, (inside the byte, bits are big endian ordered)
        for (int j = 7; j >= 0; --j) {

            // Print a character 1 or 0, given the bit value
            printf("%c", (byte[i] >> j) & 1 ? '1' : '0');
        }

        // Separate bytes
        printf(" ");
    }

    // End with a new line
    printf("\n");
}
