//
// Created by C22Bryson.Fraelich on 11/15/2019.
//
#include <stdio.h>

int alternatingSum(int num);

int pos_or_neg(int num);

int flip_bit(int num, int bit);

int main() {

    int x = 5;
    printf("%d alternating sum is %d\n", x, alternatingSum(x));
    int pos_neg = pos_or_neg(x);
    if (pos_neg == 0) {
        printf("%d, using bitwise operators, is positive\n", x);
    } else {
        printf("%d, using bitwise operators, is negative\n", x);
    }

    return 0;
}

/*
 * Takes a number and counts/adds from 1 to num, where every odd number
 * is negative and every even number is even.
 */

int alternatingSum(int num) {

    if (num == 1) {
        return -1;
    }

    if (num % 2 == 0) {
        return num + alternatingSum(num - 1);
    } else {
        return -num + alternatingSum(num - 1);
    }
}

/*
 * Bitwise operation to determine if the number is positive or negative
 */

int pos_or_neg(int num) {
    return ((num >> 31) & 1);   // if return 1 == (-) && return 0 == (+)
    // (~) is a not and flips what ever comes out
}

int flip_bit(int num, int bit){
    return(num ^ (1 << bit)); // (<<) is used to shift (1) over && (^) is used to flip the bit
}