/** lab35.c
* ===========================================================
* Name: BRYSON FRAELICH, 11.17.19
* Section: T1/2A
* Documentation: None.
* ===========================================================  */

#include "lab35.h"

int main() {
    // Exercise 1
    printf("\nExercise 1: sum_squares()\n\n");
    
    int x = 5;
    int calc = sum_squares(x);

    printf("The sum of the square of %d is %d\n", x, calc);
    

    // Exercise 2
    printf("\nExercise 2: is_even()\n\n");
    
    printf("%d's first bit returns %d\n", x, is_even(x));


    // Exercise 3
    printf("\nExercise 3: power_two()\n\n");
    
    printf("2 to the power of %d is %d\n", x, power_two(x));


    // Exercise 4
    printf("\nExercise 4: reverse_bytes()\n\n");


    // Exercise 5
    printf("\nExercise 5: is_palindrome()\n\n");
    char string[] = "racecar";
    int len = strlen(string);

    if (is_palindrome(string, len) == 1){
        printf("%s is palindrome\n", string);
    } else {
        printf("%s is not palindrome\n", string);
    }


    return 0;
}

int sum_squares(int N){
    if(N == 1){
        return 1;
    } else {
        return pow(N, 2) + sum_squares(N - 1);
    }
}

int is_even(int x){
    return ((x >> 0) & 1) ^ 1;
}

int power_two(int N){
    return 1 << N;
}

int reverse_bytes(unsigned int val){
    int new = 0;
    int temp = 0xFF;

    new = (val << 24);
    new = new ^ (val >> 24);
    new = new ^ (((temp << 8) & val) << 8);
    new = new ^ (((temp << 16) & val) >> 8);

    return new;
}

int is_palindrome(char message[], int len){
    if (len < 2){
        return 1;
    }

    return (message[0] == message[len - 1] && is_palindrome(message + 1, len - 2));
}
